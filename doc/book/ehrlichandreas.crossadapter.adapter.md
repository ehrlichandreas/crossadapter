# EhrlichAndreas\\CrossAdapter\\Adapter

The Adapter object ist the most important sub-component of
`EhrlichAndreas\CrossAdapter`. It is responsible for adapting any code
written in or for `EhrlichAndreas\CrossAdapter` to the targeted PHP
extensions and vendor apis. In doing this, it creates an abstraction layer for
the PHP apis, which is called the `"Driver"` portion of the
`EhrlichAndreas\CrossAdapter` adapter.

## Creating an Adapter - Quickstart

Creating an adapter can simply be done by instantiating the
`EhrlichAndreas\CrossAdapter\Adapter\Adapter` class. The most common use
case, while not the most explicit, is to pass an array of configuration to the
`Adapter`.

```php
$adapter = new EhrlichAndreas\CrossAdapter\Adapter(
    $configuration,
    $driverFactory
);
```

This driver array is an abstraction for the extension level required parameters.
Here is a table for the key-value pairs that should be in configuration array.

> ## Note
Other names will work as well. Effectively, if the PHP manual uses a particular
naming, this naming will be supported by our Driver. Which format you chose is
up to you, but the above table represents the official abstraction names.

So, for example, a REST connection (in pseudo-code):

```php
use EhrlichAndreas\CrossAdapter\Adapter;
use EhrlichAndreas\CrossAdapter\Driver;

$configurationArray = [
    'namespace' => 'EhrlichAndreas\CrossAdapter\Driver',
    'driver'    => 'rest',
    'option'    => [
        ...
    ],
];

$configuration = new Driver\DriverFactoryConfiguration(
    $configurationArray
);

$driverFactory = new Driver\DriverFactory();

$adapter = new Adapter\Adapter(
    $configuration,
    $driverFactory
);
```

Another example, of a Mysql connection via PDO (in pseudo-code):

```php
use EhrlichAndreas\CrossAdapter\Adapter;
use EhrlichAndreas\CrossAdapter\Driver;

$configurationArray = [
    'namespace' => 'EhrlichAndreas\CrossAdapter\Driver',
    'driver'    => 'pdomysql',
    'option'    => [
        'dsn'      => 'mysql:...',
        'username' => 'username',
        'password' => 'password',
    ],
];

$configuration = new Driver\DriverFactoryConfiguration(
    $configurationArray
);

$driverFactory = new Driver\DriverFactory();

$adapter = new Adapter\Adapter(
    $configuration,
    $driverFactory
);
```

It is important to know that by using this style of adapter creation, the
`Adapter` will attempt to create any dependencies that were not explicitly
provided. A Driver object will be created from the configuration array provided
in the constructor. This object can be injected, to do this, see the next
section.
