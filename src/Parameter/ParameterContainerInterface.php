<?php

namespace EhrlichAndreas\CrossAdapter\Parameter;

use ArrayAccess;
use Countable;
use /** @noinspection PhpUndefinedClassInspection */
    Interop\Container\ContainerInterface;
use Iterator;

/**
 * Interface ParameterContainerInterface
 *
 * @package   EhrlichAndreas\CrossAdapter\Parameter
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */

/** @noinspection PhpUndefinedClassInspection */
interface ParameterContainerInterface extends
    Iterator,
    ArrayAccess,
    Countable,
    ContainerInterface
{

}
