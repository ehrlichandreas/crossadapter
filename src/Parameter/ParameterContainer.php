<?php

namespace EhrlichAndreas\CrossAdapter\Parameter;

use EhrlichAndreas\CrossAdapter\Exception\InvalidArgumentException;

/**
 * Class ParameterContainer
 *
 * @package   EhrlichAndreas\CrossAdapter\Parameter
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
class ParameterContainer implements
    ParameterContainerInterface
{

    /**
     * Data
     *
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $positions = [];

    /**
     * Constructor
     * Enforces that we have an array, and enforces
     * parameter access to array elements.
     *
     * @param array $values parameter array
     */
    public function __construct(array $values = null)
    {
        if (null === $values) {
            $values = [];
        }

        $this->setFromArray($values);
    }

    /**
     * Returns the value at the specified index if set,
     * otherwise the defined default value
     *
     * @param string $name key name string
     *
     * @return mixed
     */
    public function get($name)
    {
        return $this->offsetGet($name);
    }

    /**
     * Returns true  if a value is set at the specified
     * index, otherwise false
     *
     * @param string $name key name string
     *
     * @return boolean
     */
    public function has($name)
    {
        return $this->offsetExists($name);
    }

    /**
     * Sets the value at the specified index to new value
     *
     * @param string $name  key name string
     * @param mixed  $value value content
     *
     * @return $this
     */
    public function set($name, $value)
    {
        $this->offsetSet($name, $value);

        return $this;
    }

    /**
     * Offset exists
     *
     * @param  string $name
     *
     * @return bool
     */
    public function offsetExists($name)
    {
        return array_key_exists($name, $this->data);
    }

    /**
     * Offset get
     *
     * @param  string $name
     *
     * @return mixed
     */
    public function offsetGet($name)
    {
        $value = null;

        if ($this->offsetExists($name)) {
            $value = $this->data[$name];
        }

        return $value;
    }

    /**
     * @param $name
     * @param $from
     */
    public function offsetSetReference($name, $from)
    {
        $this->data[$name] =& $this->data[$from];
    }

    /**
     * Returns the column/value data as an array.
     *
     * @return array
     */
    public function __toArray()
    {
        return $this->toArray();
    }

    /**
     * Returns the column/value data as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->getNamedArray();
    }

    /**
     * Populate from native PHP array
     *
     * @param array $values parameter array
     *
     * @return $this
     */
    public function setFromArray(array $values)
    {
        foreach ($values as $n => $v) {
            $this->offsetSet($n, $v);
        }

        return $this;
    }

    /**
     * Return the current element
     *
     * @link  http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return current($this->data);
    }

    /**
     * Move forward to next element
     *
     * @link  http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        /** @noinspection PhpInconsistentReturnPointsInspection */
        return next($this->data);
    }

    /**
     * Return the key of the current element
     *
     * @link  http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return key($this->data);
    }

    /**
     * Checks if current position is valid
     *
     * @link           http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then
     *                 evaluated. Returns true on success or false on failure.
     * @since          5.0.0
     */
    public function valid()
    {
        return ($this->current() !== false);
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @link  http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        reset($this->data);
    }

    /**
     * Offset to set
     *
     * @link  http://php.net/manual/en/arrayaccess.offsetset.php
     *
     * @param mixed $offset <p>The offset to assign the value to.</p>
     * @param mixed $value  <p>The value to set.</p>
     *
     * @throws InvalidArgumentException
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $position = false;

        // if integer, get name for this position
        if (is_int($offset)) {
            if (isset($this->positions[$offset])) {
                $position = $offset;
                $offset   = $this->positions[$offset];
            } else {
                $offset = (string)$offset;
            }
        } elseif (is_string($offset)) {
            // is a string:
            $position = array_key_exists($offset, $this->data);
        } elseif ($offset === null) {
            $offset = (string)$this->count();
        } else {
            throw new InvalidArgumentException(
                'Keys must be string, integer or null'
            );
        }

        if ($position === false) {
            $this->positions[] = $offset;
        }

        $this->data[$offset] = $value;
    }

    /**
     * Offset to unset
     *
     * @link  http://php.net/manual/en/arrayaccess.offsetunset.php
     *
     * @param mixed $offset <p>
     *                      The offset to unset.
     *                      </p>
     *
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        if (is_int($offset) && isset($this->positions[$offset])) {
            $offset = $this->positions[$offset];
        }

        unset($this->data[$offset]);

        /** @noinspection PhpInconsistentReturnPointsInspection */
        return $this;
    }

    /**
     * Count elements of an object
     *
     * @link  http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     *        </p>
     *        <p>
     *        The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return count($this->data);
    }

    /**
     * getNamedArray
     *
     * @return array
     */
    public function getNamedArray()
    {
        return (array)$this->data;
    }

    /**
     * getNamedArray
     *
     * @return array
     */
    public function getPositionalArray()
    {
        $namedArray      = $this->getNamedArray();
        $positionalArray = array_values($namedArray);

        return $positionalArray;
    }

    /**
     * @param array|ParameterContainer $parameters
     *
     * @throws InvalidArgumentException
     * @return ParameterContainer
     */
    public function merge($parameters)
    {
        if (!is_array($parameters) && !$parameters instanceof ParameterContainer) {
            throw new InvalidArgumentException(/** @lang text */
                '$parameters must be an array or an instance of ParameterContainer'
            );
        }

        if (count($parameters) == 0) {
            return $this;
        }

        if ($parameters instanceof ParameterContainer) {
            $parameters = $parameters->getNamedArray();
        }

        foreach ($parameters as $key => $value) {
            if (is_int($key)) {
                $key = null;
            }

            $this->offsetSet($key, $value);
        }

        return $this;
    }
}
