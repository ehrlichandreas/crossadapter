<?php

namespace EhrlichAndreas\CrossAdapter\Driver;

use EhrlichAndreas\CrossAdapter\Exception\InvalidArgumentException;
use EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer;

/**
 * Class DriverFactoryConfiguration
 *
 * @package   EhrlichAndreas\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
class DriverFactoryConfiguration extends ParameterContainer implements
    DriverFactoryConfigurationInterface
{

    const KEY_CLASSNAME = 'classname';

    const KEY_NAMESPACE = 'namespace';

    const KEY_DRIVER    = 'driver';

    const KEY_OPTIONS   = 'options';

    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * Populate from native PHP array
     *
     * @param array $values parameter array
     *
     * @return void
     * @throws InvalidArgumentException
     */
    public function setFromArray(array $values)
    {
        if (array_key_exists(self::KEY_CLASSNAME, $values)) {
            $this->setClassName($values[self::KEY_CLASSNAME]);
        }

        if (array_key_exists(self::KEY_NAMESPACE, $values)) {
            $this->setNameSpace($values[self::KEY_NAMESPACE]);
        }

        if (array_key_exists(self::KEY_DRIVER, $values)) {
            $this->setDriver($values[self::KEY_DRIVER]);
        }

        if (array_key_exists(self::KEY_OPTIONS, $values)) {
            $this->setOptions($values[self::KEY_OPTIONS]);
        }
    }

    /**
     * @param string $className
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setClassName($className)
    {
        if (!is_string($className)) {
            $exception = new InvalidArgumentException(
                __FUNCTION__
                .' expects the "className" argument to a string'
            );

            throw $exception;
        }

        if (empty($className)) {
            $exception = new InvalidArgumentException(
                __FUNCTION__
                .' expects the "className" argument to a non empty string'
            );

            throw $exception;
        }

        $this->offsetSet(self::KEY_CLASSNAME, $className);

        return $this;
    }

    /**
     * @param string $nameSpace
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setNameSpace($nameSpace)
    {
        if (!is_string($nameSpace)) {
            $exception = new InvalidArgumentException(
                __FUNCTION__
                .' expects the "nameSpace" argument to a string'
            );

            throw $exception;
        }

        if (empty($nameSpace)) {
            $exception = new InvalidArgumentException(
                __FUNCTION__
                .' expects the "nameSpace" argument to a non empty string'
            );

            throw $exception;
        }

        $this->offsetSet(self::KEY_NAMESPACE, $nameSpace);

        return $this;
    }

    /**
     * @param string $driver
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setDriver($driver)
    {
        if (!is_string($driver)) {
            $exception = new InvalidArgumentException(
                __FUNCTION__
                .' expects the "driver" argument to a string'
            );

            throw $exception;
        }

        if (empty($driver)) {
            $exception = new InvalidArgumentException(
                __FUNCTION__
                .' expects the "driver" argument to a non empty string'
            );

            throw $exception;
        }

        $this->offsetSet(self::KEY_DRIVER, $driver);

        return $this;
    }

    /**
     * @param mixed $options
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setOptions($options)
    {
        if (!($options instanceof DriverConfigurationInterface)
            && !is_array($options)
        ) {
            $exception = new InvalidArgumentException(
                __FUNCTION__
                .' expects the "options" argument to an array or an instance'
                .' of the interface "'.DriverConfigurationInterface::class.'"'
            );

            throw $exception;
        }

        $this->offsetSet(self::KEY_OPTIONS, $options);

        return $this;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->offsetGet(self::KEY_CLASSNAME);
    }

    /**
     * @return boolean
     */
    public function hasClassName()
    {
        return $this->offsetExists(self::KEY_CLASSNAME);
    }

    /**
     * @return string
     */
    public function getNameSpace()
    {
        return $this->offsetGet(self::KEY_NAMESPACE);
    }

    /**
     * @return boolean
     */
    public function hasNameSpace()
    {
        return $this->offsetExists(self::KEY_NAMESPACE);
    }

    /**
     * @return string
     */
    public function getDriver()
    {
        return $this->offsetGet(self::KEY_DRIVER);
    }

    /**
     * @return boolean
     */
    public function hasDriver()
    {
        return $this->offsetExists(self::KEY_DRIVER);
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->offsetGet(self::KEY_OPTIONS);
    }

    /**
     * @return boolean
     */
    public function hasOptions()
    {
        return $this->offsetExists(self::KEY_OPTIONS);
    }
}
