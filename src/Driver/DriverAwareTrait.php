<?php

namespace EhrlichAndreas\CrossAdapter\Driver;

/**
 * Class DriverAwareTrait
 *
 * @package   EhrlichAndreas\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
trait DriverAwareTrait
{

    /**
     * @var DriverInterface
     */
    protected $driver = null;

    /**
     * @return boolean
     */
    public function hasDriver()
    {
        return (null !== $this->getDriver());
    }

    /**
     * @return DriverInterface
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param DriverInterface $driver
     *
     * @return $this
     */
    public function setDriver(DriverInterface $driver)
    {
        $this->driver = $driver;

        return $this;
    }
}
