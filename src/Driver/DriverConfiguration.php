<?php

namespace EhrlichAndreas\CrossAdapter\Driver;

use EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer;

/**
 * Class DriverConfiguration
 *
 * @package   EhrlichAndreas\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
class DriverConfiguration extends ParameterContainer implements
    DriverConfigurationInterface
{

}
