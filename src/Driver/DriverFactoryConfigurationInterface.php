<?php

namespace EhrlichAndreas\CrossAdapter\Driver;

use EhrlichAndreas\CrossAdapter\Exception\InvalidArgumentException;
use EhrlichAndreas\CrossAdapter\Parameter\ParameterContainerInterface;

/**
 * Interface DriverFactoryConfigurationInterface
 *
 * @package   EhrlichAndreas\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
interface DriverFactoryConfigurationInterface extends
    ParameterContainerInterface
{

    /**
     * @return string
     */
    public function getClassName();

    /**
     * @return boolean
     */
    public function hasClassName();

    /**
     * @param string $className
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setClassName($className);

    /**
     * @return string
     */
    public function getNameSpace();

    /**
     * @return boolean
     */
    public function hasNameSpace();

    /**
     * @param string $nameSpace
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setNameSpace($nameSpace);

    /**
     * @return string
     */
    public function getDriver();

    /**
     * @return boolean
     */
    public function hasDriver();

    /**
     * @param string $driver
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setDriver($driver);

    /**
     * @return mixed
     */
    public function getOptions();

    /**
     * @return boolean
     */
    public function hasOptions();

    /**
     * @param mixed $options
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setOptions($options);
}
