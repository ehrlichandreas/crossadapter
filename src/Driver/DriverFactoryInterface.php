<?php

namespace EhrlichAndreas\CrossAdapter\Driver;

/**
 * Interface DriverFactoryInterface
 *
 * @package   EhrlichAndreas\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
interface DriverFactoryInterface
{

    /**
     * @param DriverFactoryConfigurationInterface $config
     *
     * @return DriverInterface
     */
    public function createDriver(
        DriverFactoryConfigurationInterface $config
    );
}
