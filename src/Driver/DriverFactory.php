<?php

namespace EhrlichAndreas\CrossAdapter\Driver;

use EhrlichAndreas\CrossAdapter\Exception\InvalidArgumentException;
use ReflectionClass;

/**
 * Class DriverFactory
 *
 * @package   EhrlichAndreas\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
class DriverFactory implements DriverFactoryInterface
{

    /**
     * @param DriverFactoryConfigurationInterface $config
     *
     * @return DriverInterface
     */
    public function createDriver(
        DriverFactoryConfigurationInterface $config
    ) {
        $className = $this->createDriverClassName($config);
        $options   = [];

        if ($config->hasOptions()) {
            $options = $config->getOptions();
        }

        if (!($options instanceof DriverConfigurationInterface)) {
            $options = $this->getDriverConfiguration($options);
        }

        $reflection            = new ReflectionClass($className);
        $reflectionConstructor = $reflection->getConstructor();
        $reflectionParameter   = [$options];

        if (!is_null($reflectionConstructor)) {
            $numberOfParameters
                = $reflectionConstructor->getNumberOfParameters();
        } else {
            $numberOfParameters = 0;
        }

        if ($numberOfParameters == 0) {
            $reflectionParameter = [];
        }

        $classInstance = $reflection->newInstanceArgs($reflectionParameter);

        return $classInstance;
    }

    /**
     * @param DriverFactoryConfigurationInterface $config
     *
     * @return string
     * @throws InvalidArgumentException
     */
    protected function createDriverClassName(
        DriverFactoryConfigurationInterface $config
    ) {
        if ($config->hasClassName()) {
            $className = $config->getClassName();

            return $className;
        }

        if (!$config->hasNameSpace()) {
            $exception = new InvalidArgumentException(
                __FUNCTION__
                .' expects a "namespace" key to be present inside the'
                .' configuration'
            );

            throw $exception;
        }

        if (!$config->hasDriver()) {
            $exception = new InvalidArgumentException(
                __FUNCTION__
                .' expects a "driver" key to be present inside the'
                .' configuration'
            );

            throw $exception;
        }

        $nameSpace = $config->getNameSpace();
        $driver    = $config->getDriver();

        $nameSpace = trim($nameSpace, '\\');
        $driver    = trim(ucfirst(strtolower($driver)), '\\');

        $className = $nameSpace.'\\'.$driver.'Driver';
        $className = trim($className, '\\');

        return $className;
    }

    /**
     * @param array $options
     *
     * @return DriverConfigurationInterface
     */
    protected function getDriverConfiguration(array $options)
    {
        $driverConfigurationClass = $this->getDriverConfigurationClass();

        $reflection          = new ReflectionClass($driverConfigurationClass);
        $reflectionParameter = [$options];

        $classInstance = $reflection->newInstanceArgs($reflectionParameter);

        return $classInstance;
    }

    /**
     * @return string
     */
    protected function getDriverConfigurationClass()
    {
        return DriverConfiguration::class;
    }
}
