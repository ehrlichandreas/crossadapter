<?php

namespace EhrlichAndreas\CrossAdapter\Exception;

use /** @noinspection PhpUndefinedClassInspection */
    Interop\Container\Exception\ContainerException as InteropContainerException;

/**
 * Interface ContainerException
 *
 * @package   EhrlichAndreas\CrossAdapter\Exception
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */

/** @noinspection PhpUndefinedClassInspection */
interface ContainerException extends
    ExceptionInterface,
    InteropContainerException
{

}
