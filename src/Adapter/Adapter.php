<?php

namespace EhrlichAndreas\CrossAdapter\Adapter;

use EhrlichAndreas\CrossAdapter\Driver\DriverAwareInterface;
use EhrlichAndreas\CrossAdapter\Driver\DriverAwareTrait;
use EhrlichAndreas\CrossAdapter\Driver\DriverFactoryInterface;
use EhrlichAndreas\CrossAdapter\Driver\DriverInterface;
use EhrlichAndreas\CrossAdapter\Exception\InvalidArgumentException;
use ReflectionClass;

/**
 * Class Adapter
 *
 * @package   EhrlichAndreas\CrossAdapter\Adapter
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
class Adapter implements AdapterInterface, DriverAwareInterface
{

    use DriverAwareTrait;

    /**
     * Adapter constructor.
     *
     * @param array|AdapterConfigurationInterface|DriverInterface $driver
     * @param DriverFactoryInterface                              $driverFactory
     */
    public function __construct(
        $driver,
        DriverFactoryInterface $driverFactory = null
    ) {
        $configuration = null;

        if (is_array($driver)) {
            $configuration = $this->createAdapterConfiguration($driver);
        } elseif ($driver instanceof AdapterConfigurationInterface) {
            $configuration = $driver;
        }

        if (!($configuration instanceof AdapterConfigurationInterface)
            && !($driver instanceof DriverInterface)
        ) {
            $exception = new InvalidArgumentException(
                'The supplied or instantiated driver object does not implement '
                .DriverInterface::class.' nor '
                .AdapterConfigurationInterface::class.' nor is an array'
            );

            throw $exception;
        }

        if (!($driver instanceof DriverInterface)
            && ($configuration instanceof AdapterConfigurationInterface)
            && ($driverFactory instanceof DriverFactoryInterface)
        ) {
            $driver = $driverFactory->createDriver($configuration);
        }

        $this->setDriver($driver);
    }

    /**
     * @param array $driver
     *
     * @return AdapterConfigurationInterface
     */
    protected function createAdapterConfiguration(array $driver)
    {
        $adapterConfigurationClass = $this->getAdapterConfigurationClass();

        $reflection          = new ReflectionClass($adapterConfigurationClass);
        $reflectionParameter = [$driver];

        $classInstance = $reflection->newInstanceArgs($reflectionParameter);

        return $classInstance;
    }

    /**
     * @return string
     */
    protected function getAdapterConfigurationClass()
    {
        return AdapterConfiguration::class;
    }
}
