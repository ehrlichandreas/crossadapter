<?php

namespace EhrlichAndreasTest\CrossAdapter\Generic;

use PHPUnit_Framework_TestCase;

/**
 * Class AbstractTest
 *
 * @package   EhrlichAndreasTest\CrossAdapter\Generic
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
abstract class AbstractTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var mixed
     */
    protected $testObject = null;

    /**
     * @return mixed
     * @coversNothing
     */
    public function getTestObject()
    {
        $testObject = $this->testObject;

        return $testObject;
    }

    /**
     * @param mixed $testObject
     *
     * @return $this
     * @coversNothing
     */
    public function setTestObject($testObject)
    {
        $this->testObject = $testObject;

        return $this;
    }

    /**
     * @inheritdoc
     * @coversNothing
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @inheritdoc
     * @coversNothing
     */
    public function tearDown()
    {
        parent::tearDown();

        $this->setTestObject(null);
    }
}
