<?php

namespace EhrlichAndreasTest\CrossAdapter\Adapter;

use EhrlichAndreas\CrossAdapter\Adapter\Adapter;
use EhrlichAndreas\CrossAdapter\Adapter\AdapterConfiguration;
use EhrlichAndreas\CrossAdapter\Adapter\AdapterConfigurationInterface;
use EhrlichAndreas\CrossAdapter\Driver\DriverFactory;
use EhrlichAndreas\CrossAdapter\Exception\InvalidArgumentException;
use EhrlichAndreasTest\CrossAdapter\Driver\Asset\Null2Driver;
use EhrlichAndreasTest\CrossAdapter\Driver\Asset\NullDriver;
use EhrlichAndreasTest\CrossAdapter\Generic\AbstractTest;
use Exception;
use PHPUnit_Framework_Error;
use ReflectionMethod;

/** @noinspection PhpMissingParentCallCommonInspection
 * Class AdapterTest
 *
 * @package   EhrlichAndreasTest\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 * @method Adapter getTestObject()
 * @method $this setTestObject(Adapter $testObject)
 */
class AdapterTest extends AbstractTest
{

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Adapter\Adapter::__construct
     */
    public function testConstructor()
    {
        $className      = NullDriver::class;
        $classNameParts = explode('\\', $className);
        $driver         = array_pop($classNameParts);
        $driver         = trim($driver, '\\');
        $driver         = preg_replace('#Driver$#', '', $driver);
        $nameSpace      = implode('\\', $classNameParts);
        $nameSpace      = trim($nameSpace, '\\');
        $driveFactory   = new DriverFactory();

        $adapterConfiguration = new AdapterConfiguration();
        $adapterConfiguration->setClassName($className);

        $adapter  = new Adapter($adapterConfiguration, $driveFactory);
        $expected = $className;
        $actual   = $adapter->getDriver();

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $adapterConfiguration = [
            'classname' => $className,
        ];

        $adapter  = new Adapter($adapterConfiguration, $driveFactory);
        $expected = $className;
        $actual   = $adapter->getDriver();

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $adapterConfiguration = new AdapterConfiguration();

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            new Adapter($adapterConfiguration, $driveFactory);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $adapterConfiguration = new AdapterConfiguration();
        $adapterConfiguration->setNameSpace($nameSpace);

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            new Adapter($adapterConfiguration, $driveFactory);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $adapterConfiguration = new AdapterConfiguration();
        $adapterConfiguration->setNameSpace($nameSpace);
        $adapterConfiguration->setDriver($driver);

        $adapter  = new Adapter($adapterConfiguration, $driveFactory);
        $expected = $className;
        $actual   = $adapter->getDriver();

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $adapterConfiguration = [
            'namespace' => $nameSpace,
            'driver'    => $driver,
        ];

        $adapter  = new Adapter($adapterConfiguration, $driveFactory);
        $expected = $className;
        $actual   = $adapter->getDriver();

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $driver = $driver.'2';

        $adapterConfiguration = new AdapterConfiguration();
        $adapterConfiguration->setNameSpace($nameSpace);
        $adapterConfiguration->setDriver($driver);
        $adapterConfiguration->setOptions([]);

        $adapter  = new Adapter($adapterConfiguration, $driveFactory);
        $expected = Null2Driver::class;
        $actual   = $adapter->getDriver();

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $adapterConfiguration = [
            'namespace' => $nameSpace,
            'driver'    => $driver,
            'options'   => [],
        ];

        $adapter  = new Adapter($adapterConfiguration, $driveFactory);
        $expected = Null2Driver::class;
        $actual   = $adapter->getDriver();

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $adapterConfiguration = 'config';
        $actual               = null;
        $expected             = InvalidArgumentException::class;

        try {
            new Adapter($adapterConfiguration, $driveFactory);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $adapterConfiguration = new AdapterConfiguration();
        $adapterConfiguration->setNameSpace($nameSpace);
        $adapterConfiguration->setDriver($driver);
        $adapterConfiguration->setOptions([]);

        $actual   = null;
        $expected = PHPUnit_Framework_Error::class;

        try {
            /** @noinspection PhpParamsInspection */
            new Adapter($adapterConfiguration, 'driveFactory');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Adapter\Adapter::createAdapterConfiguration
     */
    public function testGetDriverConfiguration()
    {
        $driveFactory         = new DriverFactory();
        $adapterConfiguration = new AdapterConfiguration();
        $adapterConfiguration->setClassName(NullDriver::class);

        $testObject       = new Adapter($adapterConfiguration, $driveFactory);
        $reflectionMethod = new ReflectionMethod(
            get_class($testObject),
            'createAdapterConfiguration'
        );
        $reflectionMethod->setAccessible(true);

        $expected = AdapterConfigurationInterface::class;
        $actual   = $reflectionMethod->invokeArgs(
            $testObject,
            [[]]
        );

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Adapter\Adapter::getAdapterConfigurationClass
     */
    public function testGetAdapterConfigurationClass()
    {
        $driveFactory         = new DriverFactory();
        $adapterConfiguration = new AdapterConfiguration();
        $adapterConfiguration->setClassName(NullDriver::class);

        $testObject       = new Adapter($adapterConfiguration, $driveFactory);
        $reflectionMethod = new ReflectionMethod(
            get_class($testObject),
            'getAdapterConfigurationClass'
        );
        $reflectionMethod->setAccessible(true);

        $expected = AdapterConfiguration::class;
        $actual   = $reflectionMethod->invokeArgs(
            $testObject,
            []
        );

        $this->assertEquals($expected, $actual);
    }
}
