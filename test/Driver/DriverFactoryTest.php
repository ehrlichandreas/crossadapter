<?php

namespace EhrlichAndreasTest\CrossAdapter\Driver;

use EhrlichAndreas\CrossAdapter\Driver\DriverConfiguration;
use EhrlichAndreas\CrossAdapter\Driver\DriverConfigurationInterface;
use EhrlichAndreas\CrossAdapter\Driver\DriverFactory;
use EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration;
use EhrlichAndreas\CrossAdapter\Exception\InvalidArgumentException;
use EhrlichAndreasTest\CrossAdapter\Driver\Asset\Null2Driver;
use EhrlichAndreasTest\CrossAdapter\Driver\Asset\NullDriver;
use EhrlichAndreasTest\CrossAdapter\Generic\AbstractTest;
use Exception;
use ReflectionMethod;

/** @noinspection PhpMissingParentCallCommonInspection
 * Class DriverFactoryTest
 *
 * @package   EhrlichAndreasTest\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 * @method DriverFactory getTestObject()
 * @method $this setTestObject(DriverFactory $testObject)
 */
class DriverFactoryTest extends AbstractTest
{

    /**
     * @inheritdoc
     * @coversNothing
     */
    public function setUp()
    {
        parent::setUp();

        $testObject = $this->getTestObject();

        if (is_null($testObject)) {
            $testObject = new DriverFactory();

            $this->setTestObject($testObject);
        }
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactory::createDriver
     */
    public function testCreateAdapter()
    {
        $testObject = $this->getTestObject();

        $className      = NullDriver::class;
        $classNameParts = explode('\\', $className);
        $driver         = array_pop($classNameParts);
        $driver         = trim($driver, '\\');
        $driver         = preg_replace('#Driver$#', '', $driver);
        $nameSpace      = implode('\\', $classNameParts);
        $nameSpace      = trim($nameSpace, '\\');

        $driverFactoryConfiguration = new DriverFactoryConfiguration();
        $driverFactoryConfiguration->setClassName($className);

        $expected = $className;
        $actual   = $testObject->createDriver($driverFactoryConfiguration);

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $driverFactoryConfiguration = new DriverFactoryConfiguration();

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            $testObject->createDriver($driverFactoryConfiguration);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $driverFactoryConfiguration = new DriverFactoryConfiguration();
        $driverFactoryConfiguration->setNameSpace($nameSpace);

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            $testObject->createDriver($driverFactoryConfiguration);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $driverFactoryConfiguration = new DriverFactoryConfiguration();
        $driverFactoryConfiguration->setNameSpace($nameSpace);
        $driverFactoryConfiguration->setDriver($driver);

        $expected = $className;
        $actual   = $testObject->createDriver($driverFactoryConfiguration);

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $driver = $driver.'2';

        $driverFactoryConfiguration = new DriverFactoryConfiguration();
        $driverFactoryConfiguration->setNameSpace($nameSpace);
        $driverFactoryConfiguration->setDriver($driver);
        $driverFactoryConfiguration->setOptions([]);

        $expected = Null2Driver::class;
        $actual   = $testObject->createDriver($driverFactoryConfiguration);

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactory::createDriverClassName
     */
    public function testCreateDriverClassName()
    {
        $testObject       = $this->getTestObject();
        $reflectionMethod = new ReflectionMethod(
            get_class($testObject),
            'createDriverClassName'
        );
        $reflectionMethod->setAccessible(true);

        $className      = NullDriver::class;
        $classNameParts = explode('\\', $className);
        $driver         = array_pop($classNameParts);
        $driver         = trim($driver, '\\');
        $driver         = preg_replace('#Driver$#', '', $driver);
        $nameSpace      = implode('\\', $classNameParts);
        $nameSpace      = trim($nameSpace, '\\');

        $driverFactoryConfiguration = new DriverFactoryConfiguration();
        $reflectionMethodArgs       = [
            $driverFactoryConfiguration,
        ];

        $driverFactoryConfiguration->setClassName($className);

        $expected = $className;
        $actual   = $reflectionMethod->invokeArgs(
            $testObject,
            $reflectionMethodArgs
        );

        $this->assertEquals($expected, $actual);

        $driverFactoryConfiguration = new DriverFactoryConfiguration();
        $reflectionMethodArgs       = [
            $driverFactoryConfiguration,
        ];

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            $reflectionMethod->invokeArgs(
                $testObject,
                $reflectionMethodArgs
            );
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $driverFactoryConfiguration = new DriverFactoryConfiguration();
        $reflectionMethodArgs       = [
            $driverFactoryConfiguration,
        ];

        $driverFactoryConfiguration->setNameSpace($nameSpace);

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            $reflectionMethod->invokeArgs(
                $testObject,
                $reflectionMethodArgs
            );
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $driverFactoryConfiguration = new DriverFactoryConfiguration();
        $reflectionMethodArgs       = [
            $driverFactoryConfiguration,
        ];

        $driverFactoryConfiguration->setNameSpace($nameSpace);
        $driverFactoryConfiguration->setDriver($driver);

        $expected = $className;
        $actual   = $reflectionMethod->invokeArgs(
            $testObject,
            $reflectionMethodArgs
        );

        $this->assertEquals($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactory::getDriverConfiguration
     */
    public function testGetDriverConfiguration()
    {
        $testObject       = $this->getTestObject();
        $reflectionMethod = new ReflectionMethod(
            get_class($testObject),
            'getDriverConfiguration'
        );
        $reflectionMethod->setAccessible(true);

        $expected = DriverConfigurationInterface::class;
        $actual   = $reflectionMethod->invokeArgs(
            $testObject,
            [[]]
        );

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactory::getDriverConfigurationClass
     */
    public function testGetAdapterConfigurationClass()
    {
        $testObject       = $this->getTestObject();
        $reflectionMethod = new ReflectionMethod(
            get_class($testObject),
            'getDriverConfigurationClass'
        );
        $reflectionMethod->setAccessible(true);

        $expected = DriverConfiguration::class;
        $actual   = $reflectionMethod->invokeArgs(
            $testObject,
            []
        );

        $this->assertEquals($expected, $actual);
    }
}
