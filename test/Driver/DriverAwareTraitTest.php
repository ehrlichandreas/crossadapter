<?php

namespace EhrlichAndreasTest\CrossAdapter\Driver;

use EhrlichAndreasTest\CrossAdapter\Driver\Asset\NullDriver;
use EhrlichAndreasTest\CrossAdapter\Generic\AbstractTest;
use Exception;
use PHPUnit_Framework_Error;

/** @noinspection PhpMissingParentCallCommonInspection
 * Class DriverAwareTraitTest
 *
 * @package   EhrlichAndreasTest\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 * @method DriverAwareTraitAsset getTestObject()
 * @method $this setTestObject(DriverAwareTraitAsset $testObject)
 */
class DriverAwareTraitTest extends AbstractTest
{

    /**
     * @inheritdoc
     * @coversNothing
     */
    public function setUp()
    {
        parent::setUp();

        $testObject = $this->getTestObject();

        if (is_null($testObject)) {
            $testObject = new DriverAwareTraitAsset();

            $this->setTestObject($testObject);
        }
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverAwareTrait::hasDriver
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverAwareTrait::getDriver
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverAwareTrait::setDriver
     */
    public function testHasDriver()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->hasDriver();
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getDriver();
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $driver = new NullDriver();

        $testObject->setDriver($driver);

        $actual   = $testObject->hasDriver();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getDriver();
        $expected = $driver;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = null;
        $expected = PHPUnit_Framework_Error::class;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setDriver([]);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual   = null;
        $expected = PHPUnit_Framework_Error::class;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setDriver(null);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }
}
