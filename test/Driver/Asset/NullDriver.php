<?php

namespace EhrlichAndreasTest\CrossAdapter\Driver\Asset;

use EhrlichAndreas\CrossAdapter\Driver\DriverInterface;

/**
 * Class Nulldriver
 *
 * @package   EhrlichAndreas\CrossAdapter\Driver\Null
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 */
class NullDriver implements
    DriverInterface
{

}
