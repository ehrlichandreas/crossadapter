<?php

namespace EhrlichAndreasTest\CrossAdapter\Driver;

use EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration;
use EhrlichAndreas\CrossAdapter\Exception\InvalidArgumentException;
use EhrlichAndreasTest\CrossAdapter\Generic\AbstractTest;
use Exception;

/** @noinspection PhpMissingParentCallCommonInspection
 * Class DriverFactoryConfigurationTest
 *
 * @package   EhrlichAndreasTest\CrossAdapter\Driver
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 * @method DriverFactoryConfiguration getTestObject()
 * @method $this setTestObject(DriverFactoryConfiguration $testObject)
 */
class DriverFactoryConfigurationTest extends AbstractTest
{

    /**
     * @inheritdoc
     * @coversNothing
     */
    public function setUp()
    {
        parent::setUp();

        $testObject = $this->getTestObject();

        if (is_null($testObject)) {
            $testObject = new DriverFactoryConfiguration();

            $this->setTestObject($testObject);
        }
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::setFromArray
     */
    public function testSetFromArray()
    {
        $testObject = $this->getTestObject();

        $values = ['foo' => 'foofoo', 'foofoo' => 'foo',];

        $testObject->setFromArray($values);

        $actual   = $testObject->toArray();
        $expected = [];

        ksort($actual);
        ksort($expected);

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $values = ['foo' => 'barbar', 'barbar' => 'foo',];

        $testObject->setFromArray($values);

        $actual   = $testObject->toArray();
        $expected = [];

        ksort($actual);
        ksort($expected);

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $values = [
            'foo'       => 'barbar',
            'barbar'    => 'foo',
            'classname' => 'classname',
            'namespace' => 'namespace',
            'driver'    => 'driver',
            'options'   => [],
        ];

        $testObject->setFromArray($values);

        $actual   = $testObject->toArray();
        $expected = [
            'classname' => 'classname',
            'namespace' => 'namespace',
            'driver'    => 'driver',
            'options'   => [],
        ];

        ksort($actual);
        ksort($expected);

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            $values = [
                'foo'       => 'barbar',
                'barbar'    => 'foo',
                'classname' => '',
                'namespace' => 'namespace',
                'driver'    => 'driver',
                'options'   => 'options',
            ];

            $testObject->setFromArray($values);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            $values = [
                'foo'       => 'barbar',
                'barbar'    => 'foo',
                'classname' => 'classname',
                'namespace' => '',
                'driver'    => 'driver',
                'options'   => 'options',
            ];

            $testObject->setFromArray($values);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            $values = [
                'foo'       => 'barbar',
                'barbar'    => 'foo',
                'classname' => 'classname',
                'namespace' => 'namespace',
                'driver'    => '',
                'options'   => 'options',
            ];

            $testObject->setFromArray($values);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            $values = [
                'foo'       => 'barbar',
                'barbar'    => 'foo',
                'classname' => 'classname',
                'namespace' => 'namespace',
                'driver'    => 'driver',
                'options'   => '',
            ];

            $testObject->setFromArray($values);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::getClassName
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::hasClassName
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::setClassName
     */
    public function testClassName()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->hasClassName();
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getClassName();
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $className = 'test';

        $testObject->setClassName($className);

        $actual   = $testObject->hasClassName();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getClassName();
        $expected = $className;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setClassName([]);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setClassName(null);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setClassName('');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setClassName('0');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::getNameSpace
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::hasNameSpace
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::setNameSpace
     */
    public function testNameSpace()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->hasNameSpace();
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getNameSpace();
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $nameSpace = 'test';

        $testObject->setNameSpace($nameSpace);

        $actual   = $testObject->hasNameSpace();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getNameSpace();
        $expected = $nameSpace;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setNameSpace([]);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setNameSpace(null);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setNameSpace('');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setNameSpace('0');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::getDriver
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::hasDriver
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::setDriver
     */
    public function testDriver()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->hasDriver();
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getDriver();
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $driver = 'test';

        $testObject->setDriver($driver);

        $actual   = $testObject->hasDriver();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getDriver();
        $expected = $driver;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setDriver([]);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setDriver(null);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setDriver('');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setDriver('0');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::getOptions
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::hasOptions
     * @covers EhrlichAndreas\CrossAdapter\Driver\DriverFactoryConfiguration::setOptions
     */
    public function testOptions()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->hasOptions();
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getOptions();
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $options = [];

        $testObject->setOptions($options);

        $actual   = $testObject->hasOptions();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getOptions();
        $expected = $options;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setOptions('options');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setOptions(null);
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setOptions('');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $actual = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->setOptions('0');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);
    }
}
