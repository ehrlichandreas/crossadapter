<?php

namespace EhrlichAndreasTest\CrossAdapter\Parameter;

use EhrlichAndreas\CrossAdapter\Exception\InvalidArgumentException;
use EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer;
use EhrlichAndreasTest\CrossAdapter\Generic\AbstractTest;
use Exception;

/** @noinspection PhpMissingParentCallCommonInspection
 * Class ParameterContainerTest
 *
 * @package   EhrlichAndreasTest\CrossAdapter\Parameter
 * @author    Andreas Ehrlich <ehrlich.andreas@googlemail.com>
 * @copyright 2016-date('Y') Andreas Ehrlich
 *            <https://www.facebook.com/Ehrlich.Andreas>
 * @link      https://bitbucket.org/ehrlichandreas/crossadapter for the
 *            canonical source repository
 * @license   UNLICENSE
 * @license   https://bitbucket.org/ehrlichandreas/crossadapter/LICENSE
 *            UNLICENSE
 * @method ParameterContainer getTestObject()
 * @method $this setTestObject(ParameterContainer $testObject)
 */
class ParameterContainerTest extends AbstractTest
{

    /**
     * @inheritdoc
     * @coversNothing
     */
    public function setUp()
    {
        parent::setUp();

        $testObject = $this->getTestObject();

        if (is_null($testObject)) {
            $testObject = new ParameterContainer();

            $this->setTestObject($testObject);
        }
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::__construct
     */
    public function testConstructor()
    {
        $values     = [];
        $testObject = new ParameterContainer();

        $actual   = $testObject->toArray();
        $expected = $values;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $values     = [];
        $testObject = new ParameterContainer($values);

        $actual   = $testObject->toArray();
        $expected = $values;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $values     = ['foo' => 'bar'];
        $testObject = new ParameterContainer($values);

        $actual   = $testObject->toArray();
        $expected = $values;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::get
     */
    public function testGet()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->get('foo');
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->get('foo');
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'barbar');

        $actual   = $testObject->get('foo');
        $expected = 'barbar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::has
     */
    public function testHas()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->has('foo');
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->has('foo');
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'barbar');

        $actual   = $testObject->has('foo');
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::offsetExists
     */
    public function testOffsetExists()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->offsetExists('foo');
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->offsetExists('foo');
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'barbar');

        $actual   = $testObject->offsetExists('foo');
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::offsetGet
     */
    public function testOffsetGet()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->offsetGet('foo');
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->offsetGet('foo');
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'barbar');

        $actual   = $testObject->offsetGet('foo');
        $expected = 'barbar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::set
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::offsetSet
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::offsetUnset
     */
    public function testSet()
    {
        $testObject = $this->getTestObject();

        $testObject->set('foo', 'barbar');

        $actual   = $testObject->get('foo');
        $expected = 'barbar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->offsetSet('foo', 'barbar');

        $actual   = $testObject->get('foo');
        $expected = 'barbar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'foofoo');

        $actual   = $testObject->get('foo');
        $expected = 'foofoo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->offsetSet('foo', 'foofoo');

        $actual   = $testObject->get('foo');
        $expected = 'foofoo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->offsetSet(1, 'foofoo');

        $actual   = $testObject->get(1);
        $expected = 'foofoo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->offsetSet(1, 'barbar');

        $actual   = $testObject->get(1);
        $expected = 'barbar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->offsetSet(null, 'foo');

        $count    = $testObject->count();
        $actual   = $testObject->get($count - 1);
        $expected = 'foo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = null;
        $expected = InvalidArgumentException::class;

        try {
            $testObject->offsetSet([], 'foo');
        } catch (Exception $e) {
            $actual = $e;
        }

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $count = $testObject->count();
        $testObject->offsetUnset($count - 1);

        $actual   = $testObject->get($count - 1);
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->offsetUnset(1);

        $actual   = $testObject->get(1);
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->offsetUnset('foo');

        $actual   = $testObject->get('foo');
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::current
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::rewind
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::next
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::key
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::valid
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::count
     */
    public function testCurrent()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->count();
        $expected = 0;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->key();
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->valid();
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'barbar');

        $actual   = $testObject->count();
        $expected = 1;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->key();
        $expected = 'foo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->valid();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->current();
        $expected = 'barbar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'foofoo');

        $actual   = $testObject->count();
        $expected = 1;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->key();
        $expected = 'foo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->valid();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->current();
        $expected = 'foofoo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('bar', 'barbar');

        $actual   = $testObject->count();
        $expected = 2;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->key();
        $expected = 'foo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->valid();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->current();
        $expected = 'foofoo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->rewind();

        $actual   = $testObject->key();
        $expected = 'foo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->valid();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->current();
        $expected = 'foofoo';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->next();

        $actual   = $testObject->key();
        $expected = 'bar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->valid();
        $expected = true;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->current();
        $expected = 'barbar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->next();

        $actual   = $testObject->key();
        $expected = null;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->valid();
        $expected = false;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->current();
        $expected = false;
        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->count();
        $expected = 2;

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::offsetSetReference
     */
    public function testOffsetSetReference()
    {
        $testObject = $this->getTestObject();

        $testObject->set('foo', 'barbar');

        $actual   = $testObject->get('foo');
        $expected = 'barbar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->offsetSetReference('foooo', 'foo');

        $actual   = $testObject->get('foooo');
        $expected = 'barbar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'bar');

        $actual   = $testObject->get('foooo');
        $expected = 'bar';

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::__toArray
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::toArray
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::getNamedArray
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::getPositionalArray
     */
    public function testToArray()
    {
        $testObject = $this->getTestObject();

        $actual   = $testObject->__toArray();
        $expected = [];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->toArray();
        $expected = [];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getNamedArray();
        $expected = [];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getPositionalArray();
        $expected = [];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'barbar');

        $actual   = $testObject->__toArray();
        $expected = ['foo' => 'barbar'];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->toArray();
        $expected = ['foo' => 'barbar'];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getNamedArray();
        $expected = ['foo' => 'barbar'];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getPositionalArray();
        $expected = ['barbar'];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $testObject->set('foo', 'foofoo');

        $actual   = $testObject->__toArray();
        $expected = ['foo' => 'foofoo'];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->toArray();
        $expected = ['foo' => 'foofoo'];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getNamedArray();
        $expected = ['foo' => 'foofoo'];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $actual   = $testObject->getPositionalArray();
        $expected = ['foofoo'];

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::setFromArray
     */
    public function testFromArray()
    {
        $testObject = $this->getTestObject();

        $values = ['foo' => 'foofoo', 'foofoo' => 'foo',];

        $testObject->setFromArray($values);

        $actual   = $testObject->toArray();
        $expected = $values;

        ksort($actual);
        ksort($expected);

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $values = ['foo' => 'barbar', 'barbar' => 'foo',];

        $testObject->setFromArray($values);

        $actual   = $testObject->toArray();
        $expected = ['foo' => 'barbar', 'barbar' => 'foo', 'foofoo' => 'foo',];

        ksort($actual);
        ksort($expected);

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }

    /** @noinspection LongLine
     * @covers EhrlichAndreas\CrossAdapter\Parameter\ParameterContainer::merge
     */
    public function testMerge()
    {
        $testObject = $this->getTestObject();

        $values = ['foo' => 'foofoo', 'foofoo' => 'foo',];

        $testObject->setFromArray($values);

        $actual   = $testObject->toArray();
        $expected = $values;

        ksort($actual);
        ksort($expected);

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $exception = null;

        try {
            /** @noinspection PhpParamsInspection */
            $testObject->merge(((object)[]));
        } catch (Exception $e) {
            $exception = $e;
        }

        $actual   = $exception;
        $expected = InvalidArgumentException::class;

        $this->assertNotNull($actual);
        $this->assertInstanceOf($expected, $actual);

        $values = ['foo' => 'foofoo', 'foofoo' => 'foo',];

        $testObject->merge([]);

        $actual   = $testObject->toArray();
        $expected = $values;

        ksort($actual);
        ksort($expected);

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $values = ['foo' => 'barbar', 'barbar' => 'foo', 0 => '23', 10 => 24];

        $testObject->merge($values);

        $actual   = $testObject->toArray();
        $expected = [
            'foo'    => 'barbar',
            'barbar' => 'foo',
            'foofoo' => 'foo',
            3        => '23',
            4        => 24,
        ];

        ksort($actual);
        ksort($expected);

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);

        $parameterContainer = new ParameterContainer([5]);

        $testObject->merge($parameterContainer);

        $actual   = $testObject->toArray();
        $expected = [
            'foo'    => 'barbar',
            'barbar' => 'foo',
            'foofoo' => 'foo',
            3        => '23',
            4        => 24,
            5        => 5,
        ];

        ksort($actual);
        ksort($expected);

        $this->assertEquals($expected, $actual);
        $this->assertSame($expected, $actual);
    }
}
